Microscope class
=======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: openflexure_microscope
    :members:
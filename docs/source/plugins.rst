Developing Plugins
=======================================================

.. toctree::
   :maxdepth: 2

   ./plugins/introduction.rst
   ./plugins/structure.rst
   ./plugins/routes.rst
   ./plugins/hardware.rst
   ./plugins/example.rst
   ./plugins/class.rst
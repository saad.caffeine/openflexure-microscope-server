Classes and Modules
===================

Plugin class
------------
.. autoclass:: openflexure_microscope.plugins.MicroscopePlugin
    :members:

.. autoclass:: openflexure_microscope.api.v1.views.MicroscopeViewPlugin
    :members:

Task module
-----------
.. automodule:: openflexure_microscope.task
    :members:

Lock module
-----------
.. automodule:: openflexure_microscope.lock
    :members:

Default plugins
---------------

Autofocus
+++++++++
.. automodule:: openflexure_microscope.plugins.default.autofocus
    :members:

Camera calibration
++++++++++++++++++
.. automodule:: openflexure_microscope.plugins.default.camera_calibration
    :members:

Scan and Stack
++++++++++++++
.. automodule:: openflexure_microscope.plugins.default.scan
    :members: